function beat_test(F)
% beat_test(F)
%   Test the beat tracker against a set of files with ground truth.
%   Each cell of cell array <F> is a sound file.  Run the beat
%   tracker, then score it against a ground truth file with the
%   same name as <n> but a ".txt" extension (mirex06 format).
% 2012-03-27 Dan Ellis dpwe@ee.columbia.edu

% Make sure it's a cell array
if ischar(F)
  F = {F};
end

nf = length(F);

% Global fixup
offset = 0.0;

for i = 1:nf
  
  wavfile = F{i};
  [p,n,e] = fileparts(wavfile);
  txtfile = fullfile(p, [n,'.txt']);
  
  beats = beat_track(wavfile);

  sys_tempo = 60 / median(diff(beats));
  
  [truth, true_tempo] = beat_ground_truth(txtfile);
  
  fprintf(1, 'Error for %s (tempo est=%5.1f, users=%5.1f BPM, %d true tracks): ', ...
          n, sys_tempo, true_tempo, length(truth));
  % Complete the report from the printout from beat_score
  collar = 0.2;
  VERBOSE = 1;
  [err(i),ins(i),del(i),tru(i)] = ...
      beat_score(beats + offset, truth, collar, VERBOSE);
  
end

fprintf(1,'Overall average error: %.1f%% (%5d ins, %5d del, %5d true)\n', ...
        100*mean(err), sum(ins), sum(del), sum(tru));

