Bitbucket git repository for signal processing cup 2017
=======================================================

by VibrasticLab02 team


The team:

0. Bagus
1. Fafa
2. Hafizh
3. Fanis
4. Dika
5. Nike
6. Deni
7. Nanda
8. Tirta

Folder:

- beattrack: the main beattrack code
- prac09: from dan ellis beat track dynamic programming. www.ee.columbia.edu/~dpwe/e4896/code/prac09/prac09.zip
- pict: some picts
- ref: references used
- audio: audio files/wav
- tools: tools used

Referensi :

- Dixon, S. (2007). Evaluation of the Audio Beat Tracking System BeatRoot. Journal of New Music Research, 36 (1), 39 - 50.
- Ellis, D.P.W. (2007). Beat Tracking by Dynamic Programming. Journal of New Music Research, 36 (1), 51 - 60.
- T.T. Georgiou, “Spectral Estimation via Selective Harmonic Amplification,” IEEE Trans. on Automatic Control, 46(1): 29-42, January 2001. [BIBTEX]

- T.T. Georgiou, “Spectral analysis based on the state covariance: the maximum entropy spectrum and linear fractional parameterization,” IEEE Trans. on Automatic Control, 47(11): 1811-1823, November 2002. [BIBTEX]

Tools:

1. Matlab
2. SonicVisualizer
3. BeatRoot
4. INESC Porto Beat Tracker
