% example usage from dan ellis' beattrack

% Load a song
[d,sr] = wavread('DrumBass.wav');
% Calculate the beat times
b = beat2(d,sr);
% Resynthesize a blip-track of the same length
db = mkblips(b,sr,length(d));
% Listen to them mixed together
soundsc(d+db,sr);

t=1/sr:1/sr:length(d)/sr;

plot(t,db); hold on;
beat_plot(b); hold off;
